package org.example;

import org.example.factory.BeerFactory;
import org.example.factory.BeerFactoryImpl;
import org.example.model.Beer;

import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

/**
 * Hello world!
 *
 */
public class App 
{
    private static BeerFactory beerFactory;
    static
    {
        beerFactory=new BeerFactoryImpl();
    }
    public static Beer getBeer()
    {
        Beer beer= Beer.builder().build();
        beer.setBeerId(UUID.randomUUID().toString());
        beer.setBeerName("Cortana");
        return  beer;
    }
    public static void main( String[] args )
    {
       /*System.out.println("Hey I am at main method...");
        System.out.printf("%s",getBeer());
*/
        beerFactory.addBeer();
        Collection<Beer> c=beerFactory.getAllBeers();
        System.out.println(c);
        Iterator<Beer> iterator=c.iterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next().toString());
        }

    }
}
